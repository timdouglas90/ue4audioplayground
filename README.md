# Audio Playground #

This is where I'm playing with audio stuff inside of Unreal Engine 4.

### Loopable machine gun sound ###

I've made a sample AK47 machine gun sound effect (relatively simple blueprint), inside the Level Blueprint.

Use the Left `CTRL` key to start the loop and let go to hear the tail.